# Tutorial

This document explains how the project works.

You are encouraged to read the [Introduction](#Introduction) for context. The **Hardware** section starts by explaining the Digital/Audio boundary

**CONTENTS**

1. Hardware Design

## Introduction

This project looks to teach participants the fundamentals of RF signals. To a certain extent, RF signals are the same as audio signals, just at a higher frequency. Using audio we can here the effects more than just seeing them in the data.

> *Note*: RF is more than just higher frequency audio. Whereas audio is vibrations of air particles, RF is an EM wave.

The system will consist of one *Pi* generating an audio signal and another recieving it. A speaker will be connected to the communication path so the audio can be heard. 

## Hardware Design

We want to generate audio on one *Pi* and recieve it on another.

Audio is an analogue signal, with a continuously varying amplitude. Computers operate in the digital domain, with values either being 1 or 0. Therefore, to produce an audio signal in the real-world, we need a *Digital-to-Analogue  converter (DAC)*. On the recieve side is the opposite, an *Analogue-to-Digital converter (ADC)*.

These converters have more levels than the binary states of 1s and 0s, but as they still have to have a digital interface, the levels are discrete rather than the natural ones you get in nature. This is shown in the image below.

![Showing the conversion between analogue and digital](/Documentation-Images/D-to-A.gif) Here an analogue sine wave must be approximated to the nearest discrete level in order to be represented digitally. With 3 bits to represent the sine wave (blue trace) the steps are very large and easy to see. With 16 bits (red) the step sizes are so small that they cannot be seen on this graph, but it works in exactly the same way.

### Circuit Used

The Raspberry Pi doesn't have any built in DAC or ADC, therefore external ones are used, with communication being doine over a digital [SPI](https://en.wikipedia.org/wiki/Serial_Peripheral_Interface) interface.

### Audio Fundamentals

Audio is an analogue signal. Whereas digital information is either a *1* or *0*, analogue signals can be any amplitude.
