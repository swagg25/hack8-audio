import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft
#%matplotlib inline
plt.rcParams['figure.figsize'] = [15, 10]

#create variables for array constructions
samplingFrequency=1000 #Hz that you sample at
sampleLength=1 #time in seconds to sample.
waveFrequencys=[10,21]

#create an evenly spaced array. This is an array of time samples.
samplingXRange=np.linspace(
    start=0,#start sampling at 0 secs
    stop=sampleLength,#time to stop sampling
    num=samplingFrequency*sampleLength#number of samples # Hz x seconds = unitless
)

#Calculate the output waveform at each sample point.
#We are summing the different CWs defined in waveFrequencies.
#Within the sum function, we loop through each waveFrequency, defining it as f. 
#2pi in the sine function gives us a base frequency of 1 Hz, because maths. We then multiply it by f to increase the frequency. The x value tells us the point in the sine wave.
functionYrange=sum(np.sin(np.pi*2*samplingXRange*f) for f in waveFrequencys)

#Plotting
fig, (ax1,ax2) = plt.subplots(1,2) #Create a figure with two plots
color = 'r'
ax1.set_xlabel('time (s)')
ax1.set_ylabel('Amplitude', color=color)
ax1.plot(samplingXRange,functionYrange, color=color) #Plot x values vs y values
ax1.tick_params(axis='x', labelcolor=color)
ax1.tick_params(axis='y', labelcolor=color)

#Generate a horizontal axis with unit (Hz)
newX=np.linspace(0,samplingFrequency,(sampleLength*samplingFrequency)//2)
#FFT the function, but stop when you reach half the total number of samples. Then take the absolute, because you don't care about the negatives
newY=np.abs(fft(functionYrange))[:(sampleLength*samplingFrequency)//2]

#do plot stuff
color = 'b'
ax2.set_xlabel('Frequency (Hz)')
ax2.set_ylabel('Amplitude', color=color)
ax2.plot(newX[:50],newY[:50], color=color)
ax2.tick_params(axis='x', labelcolor=color)
ax2.tick_params(axis='y', labelcolor=color)

#final plot stuff
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.show()
