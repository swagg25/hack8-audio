# Hack8 Audio

Project from the 8th hackathon. Ultimate Idea is to get a raspberry Pi to generate audio tones that make shapes in a spectrogram. Then another Pi receives them and interprets them. Maybe using AI.

## Overview

This project looks to explore some of the fundamentals of RF, but done within the more accessable and understandable audio spectrum. The project is based around using **Raspberry Pis**, as Python libraries can be used 

### Dependent Python Libraries

In order ot use this program, the following Python libraries required. (Update as included)

```Python
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft
```

## Locked Files

No documents are currently. Locked.

The *Master* branch **CANNOT** be written to. You must create a branch.

## Tutorial

To learn more about the theory of the project, check out the [tutorial](tutorial.md).